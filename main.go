package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

func main() {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Question side: ")
	q, err := reader.ReadString('\n')
	if err != nil {
		panic("Error reading in string!")
	}
	q = strings.TrimSuffix(q, "\n")

	fmt.Print("Answer side: ")
	a, err := reader.ReadString('\n')
	if err != nil {
		panic("Error reading in string!")
	}
	a = strings.TrimSuffix(a, "\n")

	var fName string
	if len(os.Args) >= 2 {
		fName = os.Args[1]
	} else {
		fName = "german_" + time.Now().UTC().Format("2006-01-02")
	}
	appendToFile(fmt.Sprintf("/home/morgan/anki_german/%s.txt", fName), []string{q, a})
}
