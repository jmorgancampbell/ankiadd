// TODO: Edit a deck
//			List all. Prompt to fix question or answer
//			Quick edit last
//			Quick delete last?
// TODO: German noun specific?
//			Add singular and plural quickly?

package main

import (
	"encoding/csv"
	"fmt"
	"os"
)

type deck map[string]string

func appendToFile(filename string, row []string) {
	// TODO: Check if the question already exists
	// 			Any lib for fuzzy search?

	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0664)
	if err != nil {
		fmt.Println("ERROR: Couldn't create or open file for appending:", err)
		panic("Couldn't create or open file for appending.")
	}

	w := csv.NewWriter(f)
	w.Comma = '\t'

	err = w.Write(row)
	if err != nil {
		fmt.Println("ERROR: Couldn't write CSV row:", err)
		panic("Couldn't write CSV row.")
	}
	w.Flush()
}

// func newDeck() deck {
// 	emptyDeck := deck{}

// 	return emptyDeck
// }

// func openFile(filename string) *os.File {
// 	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0664)
// 	if err != nil {
// 		panic("Error opening file!")
// 	}

// 	return f
// }

// func (d deck) saveToFile(filename string) {
// 	f := openFile(filename)
// 	w := csv.NewWriter(f)
// 	w.Comma = '\t'

// 	records := [][]string{}
// 	for k, v := range d {
// 		records = append(records, []string{k, v})
// 	}

// 	err := w.WriteAll(records)
// 	if err != nil {
// 		fmt.Println("ERROR: Can't save to file:", err)
// 	}
// }

// func newDeckFromFile(filename string) deck {
// 	f := openFile(filename)
// 	reader := csv.NewReader(f)
// 	reader.Comma = '\t'

// 	cd, err := reader.ReadAll()
// 	if err != nil {
// 		panic("Couldn't read file!")
// 	}

// 	cards := deck{}
// 	for _, row := range cd {
// 		cards[row[0]] = row[1]
// 	}

// 	return cards
// }
